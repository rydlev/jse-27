package ru.t1.rydlev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.rydlev.tm.api.component.IBootstrap;
import ru.t1.rydlev.tm.api.repository.ICommandRepository;
import ru.t1.rydlev.tm.api.repository.IProjectRepository;
import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.api.repository.IUserRepository;
import ru.t1.rydlev.tm.api.service.*;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.rydlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.rydlev.tm.model.User;
import ru.t1.rydlev.tm.repository.CommandRepository;
import ru.t1.rydlev.tm.repository.ProjectRepository;
import ru.t1.rydlev.tm.repository.TaskRepository;
import ru.t1.rydlev.tm.repository.UserRepository;
import ru.t1.rydlev.tm.service.*;
import ru.t1.rydlev.tm.util.SystemUtil;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IBootstrap, IServiceLocator {

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    @NotNull
    private final static String PACKAGE_COMMANDS = "ru.t1.rydlev.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final BackupExecutor backupExecutor = new BackupExecutor(this);

    @NotNull
    private final FileScannerExecutor fileScannerExecutor = new FileScannerExecutor(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    private void initPID() {
        try {
            @NotNull final String filename = "task-manager.pid";
            @NotNull final String pid = Long.toString(SystemUtil.getPID());
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void initDemoData() {
        @NotNull final User userTest = userService.create("test", "test", "test@test.ru");
        @NotNull final String userTestId = userTest.getId();
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTestId, "DEMO PROJECT", "DEMO DESC");
        projectService.create(userTestId, "TEST PROJECT", "TEST DESC");
        projectService.create(userTestId, "EXAMPLE PROJECT", "EXAMPLE DESC");

        taskService.create(userTestId, "MEGA TASK", "MEGA DESC");
        taskService.create(userTestId, "VEGA TASK", "VEGA DESC");
        taskService.create(userTestId, "ALPHA TASK", "ALPHA DESC");
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public void run(@Nullable final String... args) {
        parseArguments(args);
        prepareStartup();
        parseCommands();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backupExecutor.start();
        fileScannerExecutor.start();
    }

    private void prepareShutdown() {
        backupExecutor.stop();
        fileScannerExecutor.stop();
        LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***");
    }

    private void parseArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArgument(arg);
    }

    private void parseCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void parseCommand(@Nullable final String command) {
        parseCommand(command, true);
    }

    public void parseCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
