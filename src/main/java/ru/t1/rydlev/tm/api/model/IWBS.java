package ru.t1.rydlev.tm.api.model;

import ru.t1.rydlev.tm.enumerated.Status;

public interface IWBS extends IHasCreated, IHasName, IHasStatus {

}
