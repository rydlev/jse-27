package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

}
