package ru.t1.rydlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.model.ICommand;
import ru.t1.rydlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show application commands.";
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

}
